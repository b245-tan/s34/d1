// loads the expressjs module into our application and save it in a variable called express.
const express = require('express');

// localhost port no.
const port = 4000;

// app is our server
// creates an application that uses and stores it as app.
const app = express();


// middleware
// express.json() is a method which allow us to handle the streaming of data and automatically parses the incoming json from our request.
app.use(express.json());

// mock database
let users = [
		{
			username: "TStark3000",
			email: "starkindustries@mail.com",
			password: "notPeterParker"
		},
		{
			username: "ThorThunder",
			email: "loveandthunder@mail.com",
			password: "iLoveStormBreaker"
		}
	]


	// express has methods to use as routes corresponding to HTTP methods
	/*
		Syntax:
			app.method(<endpoint>, function for request and response)
	*/

	// [HTTP Method GET]
	app.get("/", (request, response) => {

		// response.status = writeHead
		// response.send = write with end()
		response.status(201).send("Hello from express")
	})

	// MINI ACTIVITY
		// create a "GET" route in expressjs which will be able to send a message in the client:
		// endpoint: /greeting
		// message: "Hello from Batch245-tan"
		// status code: 201

	app.get("/greeting", (request, response) => {
		response.status(201).send("Hello from Batch245-tan")
	})
	// END OF MINI ACTIVITY



	app.get("/users", (request, response)=> {
		response.send(users);
	})


	// [HTTP Method POST]
	app.post("/users", (request, response)=> {
		let input = request.body;

		let newUser = {
			username: input.username,
			email: input.email,
			password: input.password
		}

		users.push(newUser);

		response.send(`${newUser.username} is now registered in our website with email: ${newUser.email}!`);
	})



	// [HTTP Method DELETE]
		app.delete("/users", (request, response) => {
			
			let deletedUser = users.pop();

			response.send(deletedUser);

		})



	// [HTTP Method PUT]
		app.put("/users/:index", (request, response) =>{
			let indexToBeUpdated = request.params.index
			console.log(typeof indexToBeUpdated);

			indexToBeUpdated = parseInt(indexToBeUpdated);

			if(indexToBeUpdated < users.length){
				users[indexToBeUpdated].password = request.body.password;

				response.send(users[indexToBeUpdated]);
			} else{
				response.status(404).send("Page not found. 404!")
			}

			
		})



app.listen(port, () => console.log(`Server is running at port ${port}.`))

